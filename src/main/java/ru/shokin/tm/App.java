package ru.shokin.tm;

import java.util.Scanner;

import static ru.shokin.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */

public class App {

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     * Для run используется перегрузка методов
     */

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;

        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */

    private static int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();
            default:
                return displayError();
        }
    }

    /**
     * Вывод на экран приветствия
     */

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Вывод на эркан информации о доступных командах
     *
     * @return 0
     */

    private static int displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Closing the application");
        return 0;
    }

    /**
     * Вывод на экран версии приложения
     *
     * @return 0
     */

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Вывод на экран сведений о разработчике
     *
     * @return 0
     */

    private static int displayAbout() {
        System.out.println("Developer: Elias Shokin");
        System.out.println("Email: shokin_is@nlmk.com");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @return -1
     */

    private static int displayError() {
        System.out.println("Error!!! Unknown program arguments...");
        return -1;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */

    private static int displayExit() {
        System.out.println("See you later! :)");
        System.exit(0);
        return 0;
    }

}